## What is this?
This is a small demo project showing you how you can set up a mysql database already initialized with a schema and/or data built from SQL initialization scripts.

## How do use this
First, make sure you've got Docker installed: [Install Docker](https://docs.docker.com/v17.09/engine/installation/)

Then, open a shell and do the following:
```bash
# clone this repository
git clone https://github.com/chacewells/docker-mysql-demo.git

cd docker-mysql-demo

# Build the docker image:
docker image build --tag docker-mysql-demo:latest .

# Run it
docker container run -d -p 3306:3306 --name themepark --rm docker-mysql-demo:latest
```

### Now connect to the database via MySQL Workbench with this configuration:

|Hostname |Port|Username|Password    |Default Schema|
|---------|----|--------|------------|--------------|
|127.0.0.1|3306|root    |not-a-secret|THEMEPARK     |

### Tear it down when you're done
```bash
docker container stop themepark
```

## What just happened?
1. The `Dockerfile` in this directory describes all the things needed to set up an instance of MySQL and initialize the schema.
2. A MySQL image is retrieved from the [Dockerhub MySQL Repository](https://hub.docker.com/_/mysql/).
3. The root password is configured to "not-a-secret".
4. The default database is set to "THEMEPARK".
5. The themepark.sql setup file is copied to the MySQL intialization scripts directory `/docker-entrypoint-initdb.d`; this told MySQL to initialize the schema and add initial data to the database when the container is provisioned.
6. Port 3306 is exposed (this doesn't technically need to be done, but it's nice to know the ports you can connect to).
7. You ran the image in a container named "themepark" and connected to the database via MySQL Workbench.
