FROM mysql:8.0.12
ENV MYSQL_ROOT_PASSWORD=not-a-secret \
	MYSQL_DATABASE=THEMEPARK
COPY themepark.sql /docker-entrypoint-initdb.d
EXPOSE 3306
